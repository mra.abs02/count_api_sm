# Solution 

In this project the user can hit the CountAPI and get/display the current count.
- It is assumed that your OS is Linux or Mac
- The Python version should be > 3.8
- It does not store the values in the database.

## Development procedure/steps:
To develop the webapp the following steps have been taken:
1. Started a new project using `django-admin startproject count_app`
2. Sub-divided the project into different django app for better scalability and modularity by 
   starting a new django app: `django-admin startapp count`
3. Registered the new django app in the `count_app/settings.py`. By adding it into the `INSTALLED_APPS` list.
4. Created a new view function for the main page in the `count/views.py` file. Which renders the html file from the `templates/base.html` and `templates/counter.html` pages. 
   1. The `templates/counter.html` extends the `templates/base.html`.
   2. The`templates/base.html` has the base html tags such as `<html>`, `<head>`, and `<body>`. It also links to the 
   Bootstrap v4 css styles and JQuery framework.
   3. The `templates/counter.html` file contains the Javascript logic for hitting and getting the CountApi. Each time that page is 
   loaded it fetches the counter value by accessing the `https://api.countapi.xyz/get/` endpoint. And when the `Hit!` button is pressed
   it accesses the `https://api.countapi.xyz/hit/` endpoint and increases the counter value.
5. To access the main_page view a new path for the index/main page was created in the `count/urls.py` and it has included in the 
`count_app/urls.py`.
   
The project structure is shown in the following sub-section.


### Project Structure
The project structure is shown below.
It also briefs some of the project's important files.


```shell
├── count
│    ├── admin.py
│    ├── apps.py
│    ├── __init__.py
│    ├── migrations
│    ├── models.py
│    ├── templates          # The website templates are included here
│    │    ├── base.html     # The base html template which contains styling and required libraries
│    │    └── counter.html  # The html template for the counter api + the JQuery scripts
│    ├── tests.py
│    ├── urls.py            # The website's (sub) url/path for different pages are included here   
│    └── views.py           # The view and logic for different pages of the website
├── count_app
│    ├── settings.py
│    ├── urls.py            # All the website's url/path for different pages are included here
│    └── wsgi.py    
├── db.sqlite3              
├── manage.py               # Django management shell
├── solution.md             # this current file
├── static
│    ├── css                # CSS files
│    └── js                 # JavaScript files
```

## Execute webapp

To execute the webapp three methods have been provided. Any one of the following methods can be used. To install and run the Count App.

### 1. Use Docker

1. Enter the root directory of the project
2. Build the image and the container (Optional)
```shell
docker-compose up -d
```
3. Run the webapp:

```shell
docker-compose run -d --service-ports count_app
```
*Note 1:* You may need to run the above commands using `sudo` command or as a superuser.

#### To change the port

To change the port, edit the `docker-compose.yml` file. And edit the Ports:
```yaml
ports:
    - "<host_port>:8000"
```
For example,
```yaml
ports:
    - "3000:8000"
```

#### To stop/clean up the
To stop the container:
```shell
docker-compose down
```

To remove the container:
1. Find the container name:
```shell
docker container ls
```
2. Remove the container by its name
```shell
docker rm -f <container name>
```


### 2. Use The Provided Bash Script
##### For the first time
If you are executing the webapp for the first time run the bash script called `first_time_execute.sh`:
```shell
chmod +x first_time_execute.sh   # to make the file executable
```
```shell
./first_time_execute.sh         # execute the file
```

##### Run the app after the first time
To run the app for the subsequent times execute the `runserver.sh` file.

```shell
./runserver.sh    # first make the file executable: chmod +x runserver.sh
```

*Note:* To change the port number modify the `runserver.sh` and modify the line:
`python manage.py runserver 0.0.0.0:<new_port>` and insert the new port number.
For example, 
```shell
`python manage.py runserver 0.0.0.0:3000`
```

### 3. Using plain, vanilla, Python and Django

In the root directory of the webapp,

1. Install the required dependencies:
```shell
pip install -r requirements.txt
```
2. Run the Django app on port `3000`
```shell
$ python manage.py runserver 0.0.0.0:3000
```
*Note:* To change the port number modify the `runserver.sh` and modify the line:
`python manage.py runserver 0.0.0.0:<new_port>` and insert the new port number.
For example, 
```shell
`python manage.py runserver 0.0.0.0:3000`
```

## Configuring the webapp
To change the namespace and the key for the CountAPI, modify the `settings.py` file.
It is located in the `./count_app/settings.py`.

Locate the Count API setting section and modify the following variables as needed:
```python

COUNT_API_NAMESPACE = 'count_api_namespace'  # This should be unique
COUNT_API_KEY = 'count_API_key'
COUNT_API_GET_URL = "the_url_to_the_get_endpoint"
COUNT_API_HIT_URL = "the_url_to_the_hit_endpoint"
```



## Future updates

To:
- Provide a fancy graphic using css.
- Maybe store the counts (history) in the database.