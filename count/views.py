from django.shortcuts import render
from count_app import settings


def main_page(request):
    """
    The main page view.
    It passes the get_url and hit_url to the templates which are being used by the JQuery
    to fetch and hit the CounterAPI.
    """
    return render(request, 'counter.html', {'get_url': settings.COUNT_API_GET_URL,
                                            'hit_url': settings.COUNT_API_HIT_URL})
